package game.content.actors;

import java.util.ArrayList;

import game.GameWindow;
import game.content.object.Article;
import game.content.object.BankAccount;

//The following code relies heavily on int ID's for things.
//Should I be setting default values?
//Also, variables or protected/private with methods to pass their values?

public abstract class Actor{

	int world;
	int[] position = new int[3]; //x,y,z with z as height/altitude
	
	String name;
	
	//textTheme could specify text output method used, by being passed to a primary text output method. Thoughts? 
	int textTheme = 0;
	
	int characterClass;
	
	//core statistics
	int strength = 1;
	int dexterity = 1;
	int intelligence = 1;
	int wisdom = 1;
	int charisma = 1;
	int constitution = 1;
	int willpower = 1;

// 	TODO: Move to separate physical and mental sections?
//	int physicalSpeed;
//	int mentalSpeed;
	
	//base health and armour
	int health = constitution; //should have more factors
	int armour = 0;
	
	//skills
	//What max shall we use? 100?
	//TODO: Refine and add to available skills. Add skills related to technology, magic, science, etc
	//I stole this list from Daggerfall, only removing things that obviously didn't belong
	//Maybe steal some stuff from Dwarf Fortress as well
	//Increased through what?
	int archery = 1;
	int axe = 1;
	int backstabbing = 1;
	int bluntWeapon = 1;
	int climbing = 1;
	int criticalStrike = 1;
	int dodging = 1;
	int etiquette = 1; //Feel like this should be more about the player's conversation choices than a skill value
	int handToHand = 1;
	int lockpicking = 1;
	int longBlade = 1;
	int medical = 1;
	int mercantile = 1;
	int pickpocket = 1;
	int shortBlade = 1;
	int stealth = 1;
	int streetwise = 1; //street etiquette basically
	int swimming = 1;
	int disguise = 1;
	
	//inventory
	private ArrayList<Article> inventory = new ArrayList<Article>();
	float money;
	private ArrayList<BankAccount> bankAccounts = new ArrayList<BankAccount>();
	
	//reputation
	//In theory this could be a map of some kind (key,value pairs). Maybe groupID as key, reputation as value
	
	//traits e.g. trustworthy, brave
	
	//backstory
	//TODO: Add region classes to game.content package
	//TODO: Figure out what goes here
	
	//physical and biological information
	int race;
	int gender;
	float height; //meters
	float weight; //kilograms
	int homeworld;
	
	//quests - HANG ON, this should be in the Player class
	//TODO: Create Quest class in game.content package
	//TODO: Move quests to Player class
	
	//dialogue
	String[] greetings;
	String[] farewells;
	
	//methods

	public int getInt(String variable){
		if(variable.equals("world")){return world;}
		else if(variable.equals("textTheme")){return textTheme;}
		else if(variable.equals("characterClass")){return characterClass;}
		else if(variable.equals("strength")){return strength;}
		else if(variable.equals("dexterity")){return dexterity;}
		else if(variable.equals("intelligence")){return intelligence;}
		else if(variable.equals("wisdom")){return wisdom;}
		else if(variable.equals("charisma")){return charisma;}
		else if(variable.equals("constitution")){return constitution;}
		else if(variable.equals("willpower")){return willpower;}
		else if(variable.equals("health")){return health;}
		else if(variable.equals("armour")){return armour;}
		//That's it, I give up
		else{return -1;} //This should never get called. I should probably make it throw an exception.
	}	
	
	public void init_class() {
		ArrayList<Integer> skills = new ArrayList<Integer>();
		skills.add(archery);
		//TODO THIS IS HORRIBLE!!!! SOMEONE COME UP WITH A BETTER WAY OF ASSIGNING THE SKILL VALUES!!! -N IDK?!?!? -R
		switch (this.characterClass) {
			case 1:
				this.archery += 1;
				this.axe += 1;
				this.backstabbing += 1;
				this.bluntWeapon += 1;
				this.climbing += 1;
				this.criticalStrike += 1;
				this.dodging += 1;
				this.etiquette += 1;
				this.handToHand += 1;
				this.lockpicking += 1;
				this.longBlade += 1;
				this.medical += 1;
				this.mercantile += 1;
				this.pickpocket += 1;
				this.shortBlade +=1;
				this.stealth += 1;
				this.streetwise += 1;
				this.swimming += 1;
				this.disguise += 1;
				break;
			case 2:
				this.archery += 2;
				this.axe += 2;
				this.backstabbing += 2;
				this.bluntWeapon += 2;
				this.climbing += 2;
				this.criticalStrike += 2;
				this.dodging += 2;
				this.etiquette += 2;
				this.handToHand += 2;
				this.lockpicking += 2;
				this.longBlade += 2;
				this.medical += 2;
				this.mercantile += 2;
				this.pickpocket += 2;
				this.shortBlade +=2;
				this.stealth += 2;
				this.streetwise += 2;
				this.swimming += 2;
				this.disguise += 2;
				break;
			case 3:
				this.archery += 9001;
				this.axe += 9001;
				this.backstabbing += 9001;
				this.bluntWeapon += 9001;
				this.climbing += 9001;
				this.criticalStrike += 9001;
				this.dodging += 9001;
				this.etiquette += 9001;
				this.handToHand += 9001;
				this.lockpicking += 9001;
				this.longBlade += 9001;
				this.medical += 9001;
				this.mercantile += 9001;
				this.pickpocket += 9001;
				this.shortBlade +=9001;
				this.stealth += 9001;
				this.streetwise += 9001;
				this.swimming += 9001;
				this.disguise += 9001;
				break;
		}
	}
	
	// when an article is picked up
	public void pickUp(Article art) {
		if(inventory.size() != 50) {
			this.inventory.add(art);
		}
		else {
			GameWindow.dsp_txt_nrr("nice try, but your inventory is full.");
		}
	}
	public void inv_add(Article art) {
		this.inventory.add(art);
	}
}
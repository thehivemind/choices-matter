package game.content.actors;

import game.GameWindow;
import game.Input;

public class Player extends Actor{
	
	public Player(){};
	
	public static void createCharacter(Player player) {
		player.world = 0;
		player.position[0] = 0;
		player.position[1] = 0;
		player.position[2] = 0;
		player.textTheme = 1;
		
		//TODO: Invalid input handling for many of the switches below
		
		//RACE
		//Set homeworld based on this?
		GameWindow.dsp_txt_systm("Are you a flexible blob, a fine fuzz or a sterotypical humanoid?");
		switch (Input.getAndReturn().toLowerCase()) {
		case "blob":
			player.race = 1;
		case "fuzz":
			player.race = 2;
		case "humanoid":
			player.race = 3;
		}
		
		//GENDER
		GameWindow.dsp_txt_systm("Are you male, female or genderless?");
		switch (Input.getAndReturn().toLowerCase()) {
		case "male":
			player.gender = 1;
		case "female":
			player.gender = 2;
		case "genderless":
			player.gender = 3;
		}
		
		//NAME
		GameWindow.dsp_txt_systm("What are you called?");
		player.name = Input.getAndReturn();
		GameWindow.dsp_txt_clr();
		
		//CLASS
		GameWindow.dsp_txt_systm("Greetings " + player.name);
		GameWindow.dsp_txt_systm("Are you an One, a Two or an OP?");
		//This might get ugly...
		Boolean classValid = false;
		while (classValid == false) {
			classValid = true;
			switch (Input.getAndReturn().toLowerCase()) {
				case "one": player.characterClass = 1;
					break;
				case "two": player.characterClass = 2;
					break;
				case "op": player.characterClass = 3;
					break;
				default:
					classValid = false;
					GameWindow.dsp_txt_systm("You can't be that!");
			}
		}
		player.init_class();
		GameWindow.dsp_txt_clr();
		
		//STATS
		//Should this be modified by class + race?
		//The player should be able to adjust this
		player.strength = 10;
		player.dexterity = 10;
		player.intelligence = 10;
		player.wisdom = 10;
		player.charisma = 10;
		player.constitution = 10;
		player.willpower = 10;
	}
	
}

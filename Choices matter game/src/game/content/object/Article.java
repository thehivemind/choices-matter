package game.content.object;

import java.util.ArrayList;
import game.content.actors.Actor;

public abstract class Article {
	
	protected static ArrayList<Article> Articles = new ArrayList<Article>();
	protected abstract int getId();
	
	//executes when object is picked up (duh)
	protected void onPickUp(Actor pickupee) {
		
	}
	public static Article return_item(int id) {
		if(id == -1) {
			return new TestingObject();
		}
		else {
			if(Articles.get(id) != null) {
				return Articles.get(id);
			}
			else {
				return null;
			}
		}
	}
}

package game.content.scenery;

import game.GameWindow;

public abstract class Scenery {
		protected String encntr_txt;
		
		public void encounter() {
			GameWindow.dsp_txt_nrr(this.encntr_txt);
		}
}

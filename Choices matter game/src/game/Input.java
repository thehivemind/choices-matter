package game;

import java.util.Scanner;

public class Input {
	
	//Available commands:
	private static String goNorth = "gn";
	private static String goEast = "ge";
	private static String goSouth = "gs";
	private static String goWest = "gw";
	private static String cheat;
	//Add diagonal movement commands
	
	public static Scanner source = new Scanner(System.in);
	
	public static void get(){
			while(true){
				String command = source.next();
				process(command);
			}
		}
	
	
	public static String getAndReturn(){
			String command = source.next();
			return(command);
		}
	
	public static void process(String command){
		cheat = "/.*";
		if(command.matches(cheat)) {
			Cheat.find_cheat(command);
		}

		if(command.equals(goNorth) || command.equals(goSouth) || command.equals(goWest)){
			GameWindow.dsp_txt_npc("Stop poking around at the unfinished mechanics!");
		}

		if(command.equals(goEast)) {
			GameWindow.dsp_txt_systm("HA! lol, you tried to go East. Like that was going to work.");
		}
	}
}

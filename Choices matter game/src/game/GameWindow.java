package game;

import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;

import org.fusesource.jansi.AnsiConsole;

//https://github.com/fusesource/jansi
//https://fusesource.github.io/jansi/documentation/api/index.html

//TODO: Rebuild ANSI printing to use ANSIConsole

public class GameWindow extends Game {
	//NPC dialogue
	public static void dsp_txt_npc(String text) {
		System.out.println(ansi().fg(BLUE).a(text).reset());
	}
	//meta text
	public static void dsp_txt_systm(String text) {
		System.out.println(text);
	}
	
	//Print without newline. Meta formating
	public static void dsp_txt_endls(String text) {
		System.out.print(text);
	}
	
	//narration
	public static void dsp_txt_nrr(String text) {
		System.out.println(text); 
	}
	
	//clears screen
	public static void dsp_txt_clr() {
		AnsiConsole.out.println("\u001b[2J");
	}
}

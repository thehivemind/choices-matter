package game;

/*
 * User input, text output, able to be expanded with classes, etc as different files.
 * Wrappers for input/output so we can separate display from everything else. 
*/
public class Base {
	public static void main(String args[]) {
		Game.run();
	}

}